package app

import (
	"context"
	"fmt"
	"localization/config"
	"localization/internal/consts"
	"localization/internal/controllers"
	"localization/internal/entities"
	"localization/internal/middlewares"
	"localization/internal/repo"
	"localization/internal/repo/driver"
	"localization/internal/usecases"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/patrickmn/go-cache"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"

	"gitlab.com/tuneverse/toolkit/core/logger"
	"gitlab.com/tuneverse/toolkit/middleware"
)

// Run method to run the service in gin framework
// env configuration
// logrus, zap
// use case intia
// repo initalization
// controller init
func Run() {
	// init the env config
	cfg, err := config.LoadConfig(consts.AppName)
	if err != nil {
		panic(err)
	}

	var log *logger.Logger

	// ################ For Logging to a File ################
	// Create a file logger configuration with specified settings.
	file := &logger.FileMode{
		LogfileName:  "localization.log", // Log file name.
		LogPath:      "logs",             // Log file path. Add this folder to .gitignore as logs/
		LogMaxAge:    7,                  // Maximum log file age in days.
		LogMaxSize:   1024 * 1024 * 10,   // Maximum log file size (10 MB).
		LogMaxBackup: 5,                  // Maximum number of log file backups to keep.
	}

	// ################ For Logging to a Database ################
	// Create a database logger configuration with the specified URL and secret.
	// For development purpose don't use cloudMode.

	// ############# Client Options #############
	// Configure client options for the logger.
	clientOpt := &logger.ClientOptions{
		Service:             consts.AppName, // Service name.
		LogLevel:            "info",         // Log level.
		IncludeRequestDump:  false,          // Include request data in logs.
		IncludeResponseDump: false,          // Include response data in logs.
		// JsonFormater:        false,          // log format, if sets to false, default text formatter is used.
	}
	// Check if the application is in debug mode.
	if cfg.Debug {
		// Debug Mode: Logs will print to both the file and the console.

		// Initialize the logger with the specified configurations for file and console logging.
		log = logger.InitLogger(clientOpt, file)
	} else {
		// Release Mode: Logs will print to a database, file, and console.

		// Create a database logger configuration with the specified URL and secret.
		db := &logger.CloudMode{
			// Database API endpoint (for best practice, load this from an environment variable).
			URL: cfg.LoggerServiceURL,
			// Secret for authentication.
			Secret: cfg.LoggerSecret,
		}

		// Initialize the logger with the specified configurations for database, file, and console logging.
		log = logger.InitLogger(clientOpt, db, file)
	}

	// database connection
	mongoDB, err := driver.ConnectDB(cfg.Db)
	if err != nil {
		log.Fatalf("unable to connect the database : %v", err)
		return
	}

	// // here initalizing the router
	router := initRouter()
	if !cfg.Debug {
		gin.SetMode(gin.ReleaseMode)
	}

	api := router.Group("/api")
	// middleware initialization
	api.Use(middleware.LogMiddleware(map[string]interface{}{}))
	api.Use(middleware.APIVersionGuard(middleware.VersionOptions{
		AcceptedVersions: cfg.AcceptedVersions,
	}))

	m := middlewares.NewMiddlewares(cfg)
	api.Use(m.LocalizationLanguage())
	api.Use(m.EndPointNames())

	api.Use(middleware.Localize())
	api.Use(middleware.ErrorLocalization(
		middleware.ErrorLocaleOptions{
			Cache:                  cache.New(5*time.Minute, 10*time.Minute),
			CacheExpiration:        time.Duration(time.Hour * 24),
			CacheKeyLabel:          "ERROR_CACHE_KEY_LABEL",
			LocalisationServiceURL: fmt.Sprintf("%s/localization/error", cfg.LocalisationServiceURL),
		},
	))
	// repo initialization
	repo := repo.NewLocalizationRepo(mongoDB)

	// initilizing usecases
	useCases := usecases.NewLocalizationUseCases(repo)

	// initalizin controllers
	controller := controllers.NewLocalizationController(api, useCases)

	// init the routes
	controller.InitRoutes()

	// runn the app
	launch(cfg, router)
}

func initRouter() *gin.Engine {
	router := gin.Default()
	gin.SetMode(gin.DebugMode)

	// CORS
	// - PUT and PATCH methods
	// - Origin header
	// - Credentials share
	// - Preflight requests cached for 12 hours
	router.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"PUT", "PATCH", "POST", "DELETE", "GET", "OPTIONS"},
		AllowHeaders:     []string{"Origin"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		// AllowOriginFunc: func(origin string) bool {
		// },
		MaxAge: 12 * time.Hour,
	}))

	return router
}

// launch
func launch(cfg *entities.EnvConfig, router *gin.Engine) {
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%v", cfg.Port),
		Handler: router,
	}

	go func() {
		// service connections
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()
	fmt.Println("Server listening in...", cfg.Port)
	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 5 seconds.
	quit := make(chan os.Signal)
	// kill (no param) default send syscanll.SIGTERM
	// kill -2 is syscall.SIGINT
	// kill -9 is syscall. SIGKILL but can"t be catch, so don't need add it
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Println("Shutdown Server ...")

	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}
	// catching ctx.Done(). timeout of 5 seconds.
	select {
	case <-ctx.Done():
		log.Println("timeout of 5 seconds.")
	}
	log.Println("Server exiting")
}
