package repo

import (
	"go.mongodb.org/mongo-driver/mongo"
)

// LocalizationRepo defines a repository for localization-related operations.
type LocalizationRepo struct {
	db *mongo.Database
}

type LocalizationRepoImply interface {
}

// NewLocalizationRepo creates a new instance of MemberRepo.
func NewLocalizationRepo(db *mongo.Database) LocalizationRepoImply {
	return &LocalizationRepo{db: db}
}
