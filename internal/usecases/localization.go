package usecases

import "localization/internal/repo"

type LocalizationUseCases struct {
	repo repo.LocalizationRepoImply
}

// LocalizationUseCaseImply interface
type LocalizationUseCaseImply interface {
}

// NewMemberUseCases is a constructor for creating an instance of MemberUseCases.
func NewLocalizationUseCases(localizationRepo repo.LocalizationRepoImply) LocalizationUseCaseImply {
	return &LocalizationUseCases{
		repo: localizationRepo,
	}
}
