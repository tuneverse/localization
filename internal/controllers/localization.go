package controllers

import (
	"localization/internal/usecases"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/tuneverse/toolkit/core/version"
)

type LocalizationController struct {
	router   *gin.RouterGroup
	useCases usecases.LocalizationUseCaseImply
}

// NewLocalizationController creates a new instance of LocalizationController.
func NewLocalizationController(router *gin.RouterGroup, localizationUseCase usecases.LocalizationUseCaseImply) *LocalizationController {
	return &LocalizationController{
		router:   router,
		useCases: localizationUseCase,
	}
}

func (localization *LocalizationController) InitRoutes() {
	localization.router.GET("/:version/health", func(ctx *gin.Context) {
		version.RenderHandler(ctx, localization, "HealthHandler")
	})
}

// HealthHandler handles health endpoint.
func (localization *LocalizationController) HealthHandler(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  "success",
		"message": "server run with base version",
	})
}
